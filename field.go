// Field defines the field interface and common functions.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package field

import (
	"bitbucket.org/pcasmath/object"
	"bitbucket.org/pcasmath/ring"
)

// Interface defines the field interface that all fields must satisfy.
type Interface interface {
	ring.Interface
	ring.IsUniter
	// Inverse returns the inverse x^-1 of x.
	Inverse(x object.Element) (object.Element, error)                  
	// Divide returns the division x / y.
	Divide(x object.Element, y object.Element) (object.Element, error) 
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Sum returns the sum of the elements in the slice S. The sum of the empty slice is the zero element. If F satisfies the interface:
//		type Sumer interface {
//			// Sum returns the sum of the elements in the slice S.
//			// The sum of the empty slice is the zero element.
//			Sum(S ...object.Element) (object.Element, error) 
//		}
// then F's Sum method will be called.
func Sum(F Interface, S ...object.Element) (object.Element, error) {
	return ring.Sum(F, S...)
}

// Product returns the product of the elements in the slice S. The product of the empty slice is one. If F satisfies the interface:
//		type Producter interface {
//			// Product returns the product of the elements in the
//			// slice S. The product of the empty slice is one.
//			Product(S ...object.Element) (object.Element, error)
//		}
// then F's Product method will be called.
func Product(F Interface, S ...object.Element) (object.Element, error) {
	return ring.Product(F, S...)
}
