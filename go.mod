module bitbucket.org/pcasmath/field

go 1.16

require (
	bitbucket.org/pcasmath/object v0.0.4
	bitbucket.org/pcasmath/ring v0.0.1
)
